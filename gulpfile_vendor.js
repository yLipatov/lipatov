module.exports = function (gulp, plugins) {
  var vendorDir = './node_modules/',
    vendorArr = {
      jquery: {
        JS: 'jquery/dist/jquery.min.js'
      },
      swiper: {
        CSS: 'swiper/dist/css/swiper.min.css',
        JS: 'swiper/dist/js/swiper.min.js'
      },
      fancybox: {
        CSS: '@fancyapps/fancybox/dist/jquery.fancybox.min.css',
        JS: '@fancyapps/fancybox/dist/jquery.fancybox.min.js'
      }
    },
    arr = [],
    i = 0;

  for (var prop in vendorArr) {
    arr[i] = '' + prop;
    i++;
  }

  var css = arr.map(function (arr) {
    return gulp.src(vendorDir + vendorArr[arr].CSS ? vendorDir + vendorArr[arr].CSS : [])
      .pipe(gulp.dest('dist/vendor/' + arr + '/css'));
  });

  var js = arr.map(function (arr) {
    return gulp.src(vendorDir + vendorArr[arr].JS ? vendorDir + vendorArr[arr].JS : [])
      .pipe(gulp.dest('dist/vendor/' + arr + '/js'));
  });

  return function () {
    return plugins.merge(css, js);
  };
};