module.exports = function (gulp, plugins, getFolders) {
  function onError(e) {
    console.log(e.toString());
    this.emit('end');
  }

  /* Paths */
  var src = {
      main: 'src/js/*.js',
      components: 'src/components'
    },
    dist = {
      main: 'dist/js',
      components: 'dist/js/components'
    };
  /* End Paths */

  return function () {

    gulp.src(src.main)
      .pipe(plugins.plumber({errorHandler: onError}))
      .pipe(plugins.changedInPlace({firstPass: true}))
      .pipe(gulp.dest(dist.main))
      .pipe(plugins.uglify())
      .pipe(plugins.rename({suffix: '.min'}))
      //.pipe(plugins.debug({title: 'JS:'}))
      .pipe(gulp.dest(dist.main));

    var components = getFolders(src.components).map(function (folder) {
      return gulp.src(plugins.path.join(src.components, folder, '/*.js'))
        .pipe(plugins.plumber({errorHandler: onError}))
        .pipe(plugins.concat(folder + '.js'))
        .pipe(plugins.changedInPlace({firstPass: true}))
        .pipe(gulp.dest(dist.components))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({suffix: '.min'}))
        //.pipe(plugins.debug({title: 'JS:'}))
        .pipe(gulp.dest(dist.components));
    });

    return plugins.merge(components);
  };
};